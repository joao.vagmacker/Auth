package com.auth.demo.repository;

import com.auth.demo.entity.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jean.flores
 */
public interface UserRepository extends JpaRepository<User, Integer>{
    
    @Override
    public List<User> findAll();
    
    public User findById(Integer id);
    
}
