package com.auth.demo.repository;

import com.auth.demo.entity.Profile;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jean.flores
 */
public interface ProfileRepository extends JpaRepository<Profile, Integer>{
    
    @Override
    public List<Profile> findAll();
    
    public Profile findById(int id);
    
}
