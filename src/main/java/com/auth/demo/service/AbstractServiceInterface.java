package com.auth.demo.service;

import java.util.List;

public interface AbstractServiceInterface<T> {

    void save(T obj);

    void update(T obj);

    boolean delete(int id);

    List<T> list();

    T findById(int id);

}
