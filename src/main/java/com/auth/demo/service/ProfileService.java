package com.auth.demo.service;

import com.auth.demo.entity.Profile;
import com.auth.demo.repository.ProfileRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jean.flores
 */
@Service
public class ProfileService implements AbstractServiceInterface<Profile>{
    
    @Autowired
    private ProfileRepository profileRepository;
    
    @Override
    public void save(Profile profile){
        if(profile.getId() == 0){
            validate(profile);
        }
        profileRepository.save(profile);
    }
    
    @Override
    public void update(Profile profile){
        profileRepository.save(profile);
    }    
    
    @Override
    public boolean delete(int id){
        Profile profile = profileRepository.findOne(id);
        if(profile != null){
            profileRepository.delete(id);
            return true;
        }
        return false;
    }
    
    @Override
    public List<Profile> list() {
        return profileRepository.findAll();
    }

    @Override
    public Profile findById(int id){
        Profile profile = profileRepository.findOne(id);
        if(profile == null){
            throw new RuntimeException("Perfil não existe");
        }
        return profile;
    }
    
    private void validate(Profile profile){
        if(profileRepository.findById(profile.getId()) != null){
            throw new RuntimeException(String.format("Perfil já cadastrado."));
        }
    }
}
    